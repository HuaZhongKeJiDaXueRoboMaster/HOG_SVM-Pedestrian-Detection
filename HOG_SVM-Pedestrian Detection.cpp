#include <opencv2\opencv.hpp>  
#include<iostream>
#include<stdio.h>
//#include <opencv2/gpu/gpu.hpp>  



using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
	cout << CV_VERSION << endl;
	Mat frame=imread("image1.jpg");
	vector<Rect> people;

	//定义HOG对象
	HOGDescriptor Hog;

	//设置SVM分类器  
	Hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());

	//进行检测  
	Hog.detectMultiScale(frame, people, 0, Size(8, 8), Size(0, 0), 1.03, 2);

	//画长方形，框出行人  
	for (int i = 0; i < people.size(); i++)
	{
		Rect r = people[i];
		rectangle(frame, r.tl(), r.br(), Scalar(0, 0, 255), 3);
	}

	namedWindow("检测行人", CV_WINDOW_AUTOSIZE);
	imshow("检测行人", frame);
	

	waitKey(0);
	return 0;
}